package com.hd;

import com.hd.entity.doc.Document;
import com.hd.util.ElasticsearchClentUtil;
import com.hd.util.ElasticsearchResponseEntity;
import com.hd.util.JsonUtil;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * hzhh123
 * 2019/3/28 11:45
 */
public class TestElastic {

    @Test
    public void test1(){
        ElasticsearchClentUtil<Document> elasticsearchClentUtil = new ElasticsearchClentUtil<>("esmapper/search.xml");
        Map<String,String> paramsMap = new HashMap<String,String>();
        paramsMap.put("from","0");
        paramsMap.put("size","10");
        paramsMap.put("classicId","00");
        paramsMap.put("keywords","建设");
        ElasticsearchResponseEntity<Document> documentElasticsearchResponseEntity = elasticsearchClentUtil.searchDocumentByKeywords("/document/document/_search","searchPagineDatas",
                paramsMap,Document.class);
        System.out.println(JsonUtil.toJson(documentElasticsearchResponseEntity));
    }

    @Test
    public void test2(){
        ElasticsearchResponseEntity<Document> documentElasticsearchResponseEntity = queryDoc(1,5,"00","建设");
        System.out.println(JsonUtil.toJson(documentElasticsearchResponseEntity));
    }

    public ElasticsearchResponseEntity<Document> queryDoc(int page,int size,String classicId,String keywords){
        String from = "0";
        if (page < 1){
            from = "0";
        }else {
            from =""+(page - 1)*size;
        }
        Map<String,String> paramsMap = new HashMap<String,String>();
        paramsMap.put("from",from);
        paramsMap.put("size",""+size);
        paramsMap.put("classicId",classicId);
        paramsMap.put("keywords",keywords);
        ElasticsearchClentUtil<Document> elasticsearchClentUtil = new ElasticsearchClentUtil<>("esmapper/search.xml");
        ElasticsearchResponseEntity<Document> documentElasticsearchResponseEntity = elasticsearchClentUtil.searchDocumentByKeywords("/document/document/_search","searchPagineDatas",
                paramsMap,Document.class);
        return documentElasticsearchResponseEntity;
    }
}
