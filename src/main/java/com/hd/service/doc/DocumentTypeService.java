package com.hd.service.doc;

import com.hd.entity.doc.Document;
import com.hd.entity.doc.DocumentType;

import java.util.List;

/**
 * hzhh123
 * 2019/3/27 14:09
 * @description 文件类型
 */
public interface DocumentTypeService {
    /**
     * @description 获取父类pid空的文档类型
     * @return
     */
    public List<DocumentType> getParents();

    /**
     * @description 通过pid查询子类文档类型集合
     * @param pid
     * @return
     */
    public List<DocumentType> getChilds(String pid);
}
