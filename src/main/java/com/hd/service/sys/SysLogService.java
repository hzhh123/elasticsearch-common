package com.hd.service.sys;

import com.hd.entity.sys.SysLog;
import com.hd.util.result.PageInfo;

/**
 * @author hzhh123
 * @time 2018年1月30日上午10:56:08
 **/
public interface SysLogService {
	void save(SysLog sysLog);
	void delete(String id);
	SysLog get(String id);
	void selectDataGrid(PageInfo pageInfo, SysLog sysLog);
}
