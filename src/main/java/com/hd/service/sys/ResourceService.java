package com.hd.service.sys;

import java.util.List;

import com.hd.commons.shiro.ShiroUser;
import com.hd.entity.sys.Resource;
import com.hd.util.result.Tree;

/**
 * @author hzhh123
 * @time 2018年1月15日上午10:30:53
 **/
public interface ResourceService {
	 List<Resource> selectAll(Resource resource);

	    List<Tree> selectAllMenu();

	    List<Tree> selectAllTree(String roleId);

	    List<Tree> selectTree(ShiroUser shiroUser, Integer resourceType);

		void deleteById(String id);

		void updateById(Resource resource);

		Resource selectById(String id);

		void insert(Resource resource);

		List<Resource>getByNameLike(String name);
		List<Tree>tree(String resIds);
		List<Resource>getByIds(String resIds);


	
}
