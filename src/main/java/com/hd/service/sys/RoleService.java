package com.hd.service.sys;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.hd.entity.sys.Role;
import com.hd.util.result.PageInfo;

/**
*@author hzhh123
*@time 2018年1月15日上午10:30:16 
**/
public interface RoleService {
	void selectDataGrid(PageInfo pageInfo);

    Object selectTree();

    List<String> selectResourceIdListByRoleId(String id);

    void updateRoleResource(String id, String resourceIds);

	void insert(Role role);

	void deleteById(String id);

	Role selectById(String id);

	void updateById(Role role);
	Map<String, Set<String>> selectResourceMapByUserId(String userId);
	List<Role>findbyName(String name);
	
	String getUserids(String roleids);

}
