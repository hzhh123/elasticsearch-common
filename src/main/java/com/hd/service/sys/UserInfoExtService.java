package com.hd.service.sys;
import com.hd.entity.sys.UserInfoExt;
public interface UserInfoExtService {
    void save(UserInfoExt userInfoExt);
    void delete(String objId);
    UserInfoExt getByObjId(String objId);

}
