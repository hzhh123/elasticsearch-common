package com.hd.commons.scan;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
*@author hzhh123
*@time 2018年1月30日上午10:06:56 
**/
@Component("jacksonObjectMapper")
public class JacksonObjectMapper extends ObjectMapper {

}
