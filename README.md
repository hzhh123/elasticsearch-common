# elasticsearch-common

#### 介绍
 springmvc集成Elasticsearch实现一个简单的全文检索系统，系统包含tika提取文件内容，hanlp进行自然分词、提取短语关键字等

#### 软件架构
springmvc+shiro+hibernate+mysql+elasticsearch




#### 使用说明

1. 具体集成过程参考[https://www.cnblogs.com/hzhh123/p/10635251.html](https://www.cnblogs.com/hzhh123/p/10635251.html)


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)